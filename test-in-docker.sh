#!/bin/bash

rustc --version && cargo --version
echo ""
echo "Test in headless Chromium"
CHROMEDRIVER=`pwd`/chromedriver cargo test --target wasm32-unknown-unknown --manifest-path=/simi/simi/Cargo.toml

echo ""
echo "Build all examples"
TARGET="--target=wasm32-unknown-unknown"
cargo +nightly build $TARGET --manifest-path=/simi/simi/examples/a-counter/Cargo.toml
cargo +nightly build $TARGET --manifest-path=/simi/simi/examples/b-counter-snippet/Cargo.toml
cargo +nightly build $TARGET --manifest-path=/simi/simi/examples/c-counter-nested-snippet/Cargo.toml
cargo +nightly build $TARGET --manifest-path=/simi/simi/examples/d-clock-component/Cargo.toml
cargo +nightly build $TARGET --manifest-path=/simi/simi/examples/e-clock-sub-app/Cargo.toml