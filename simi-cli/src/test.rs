use clap;

use config::SimiConfig;
use error::*;
use external_cmds as cmds;

pub struct TestArgs {
    pub with_head: bool,
}

impl TestArgs {
    pub fn from_clap<'a>(matches: &clap::ArgMatches<'a>) -> Self {
        Self {
            with_head: matches.is_present("with_head"),
        }
    }
}

pub fn run(args: TestArgs) -> Result<(), Error> {
    let config = SimiConfig::for_test(args)?;
    if config.has_browser_driver() == false {
        return Err(format_err!("You must specify at least one browser driver in `simi.toml` (at the same place as `Cargo.toml`)"));
    }
    //let nightly = true;
    //cmds::cargo_build(config.release(), nightly, config.target())?;
    for driver_path in config.browser_drivers() {
        println!("Test with {}", driver_path);
        cmds::cargo_test(&config, driver_path)?;
    }
    Ok(())
}
