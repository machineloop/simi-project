use error::*;
use external_cmds as cmds;

pub fn run() -> Result<(), Error> {
    cmds::cargo_expand()?;
    Ok(())
}
