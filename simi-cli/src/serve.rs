use clap;
//use notify;

use build;
use build::BuildArgs;
use config::SimiConfig;
use error::*;

pub struct ServeArgs {
    pub build: BuildArgs,
    pub port: u16,
}

impl ServeArgs {
    pub fn from_clap<'a>(matches: &clap::ArgMatches<'a>) -> Self {
        let build = BuildArgs::from_clap(matches);
        let port: u16 = match matches.value_of("port") {
            Some(port) => port.parse().expect("Unable to parse port number"),
            _ => ::DEFAULT_SERVE_PORT,
        };
        Self { build, port }
    }
}

pub fn run(arg: ServeArgs) -> Result<(), Error> {
    let config = SimiConfig::from_serve(arg)?;
    // First build
    build::build(&config)?;

    let sfs = ::simple_static_server::StaticFiles::new(
        &config.output_path().to_string_lossy().to_string(),
    )?;
    sfs.start(config.serve_port());
    Ok(())
}
