use config::{SimiConfig, SimiStage};
use error::*;
use std::process::Command;

pub fn cargo_build(release: bool, nightly: bool, target: &str) -> Result<(), Error> {
    let mut cargo = Command::new("cargo");
    if nightly {
        cargo.arg("+nightly");
    }
    cargo.arg("build").arg("--target").arg(target);

    if release {
        cargo.arg("--release");
    }

    //println!("{:?}", cargo);
    if cargo.status()?.success() {
        return Ok(());
    };
    Err(format_err!("cargo build failed"))
}

//cargo +nightly rustc -- -Z unstable-options --pretty=expanded
pub fn cargo_expand() -> Result<(), Error> {
    let mut cargo = Command::new("cargo");
    cargo.args(&[
        "+nightly",
        "rustc",
        "--target",
        ::DEFAULT_TARGET,
        "--",
        "-Z",
        "unstable-options",
        "--pretty=expanded",
    ]);
    //println!("{:?}", cargo);
    if cargo.status()?.success() {
        return Ok(());
    };
    Err(format_err!("{:?} failed", cargo))
}

pub fn cargo_test(config: &SimiConfig, driver_path: &str) -> Result<(), Error> {
    let driver_name = driver_path
        .split('/')
        .last()
        .expect("Invalid value for browser driver")
        .to_uppercase();

    let mut cargo = Command::new("cargo");
    cargo.env(driver_name, driver_path);

    if config.with_head() {
        cargo.env("NO_HEADLESS", "1");
    }
    cargo.arg("+nightly");
    cargo.arg("test");
    cargo.arg("--target").arg(config.target());

    println!("{:?}", cargo);
    if cargo.status()?.success() {
        return Ok(());
    };
    Err(format_err!("{:?} failed", cargo))
}
/*
pub fn wb_test(config: &SimiConfig, driver_path: &str) -> Result<(), Error> {
    let mut wb_test = Command::new("wasm-bindgen-test-runner");
    let driver_name = driver_path
        .split('/')
        .last()
        .expect("Invalid value for browser driver")
        .to_uppercase();
    wb_test.env(driver_name, driver_path);

    if config.with_head() {
        wb_test.env("NO_HEADLESS", "1");
    }

    wb_test.arg(config.get_wasm_file_path(SimiStage::CargoBuild));

    println!("{:?}", wb_test);
    if wb_test.status()?.success() {
        return Ok(());
    };
    Err(format_err!("{:?} failed", wb_test))
}
*/
pub fn wasm_bindgen(config: &SimiConfig) -> Result<(), Error> {
    let mut wb = Command::new("wasm-bindgen");
    wb.arg(config.get_wasm_file_path(SimiStage::CargoBuild))
        .arg("--browser")
        //.arg("--no-modules")
        .arg("--no-typescript")
        .arg("--out-dir")
        .arg(config.output_path().to_string_lossy().to_string());

    println!("{:?}", wb);
    if wb.status()?.success() {
        return Ok(());
    }
    Err(format_err!("wasm-bindgen failed"))
}

pub fn wasm_to_es6js(config: &SimiConfig) -> Result<(), Error> {
    let mut w2js = Command::new("wasm2es6js");
    w2js.arg(config.get_wasm_file_path(SimiStage::SimiFinalApp))
        .arg("--fetch")
        .arg(format!(
            "{}/{}",
            config.wasm_serve_path(),
            config.get_wasm_file_name(true)
        ))
        .arg("-o")
        .arg(config.get_wasm_loader_js());
    println!("{:?}", w2js);
    if w2js.status()?.success() {
        return Ok(());
    }
    Err(format_err!("wasm2es6js failed"))
}

// I just don't want to copy files again if they are not modified
#[cfg(target_os = "linux")]
pub fn copy<P1, P2>(from: &P1, to: &P2) -> Result<(), Error>
where
    P1: AsRef<str> + ::std::fmt::Debug,
    P2: AsRef<str>,
{
    let mut cp = Command::new("cp");
    cp.arg("-u") // Update only
        .arg(from.as_ref())
        .arg(to.as_ref());

    println!("{:?}", cp);
    if cp.status()?.success() {
        return Ok(());
    }
    Err(format_err!(
        "copy failed: {} => {}",
        from.as_ref(),
        to.as_ref()
    ))
}

#[cfg(not(target_os = "linux"))]
pub fn copy<P1, P2>(from: &P1, to: &P2) -> Result<(), Error>
where
    P1: AsRef<str> + ::std::fmt::Debug,
    P2: AsRef<str>,
{
    println!("Copy: {:?}", from);
    let from = Path::new(from.as_ref());
    let to = Path::new(to.as_ref());
    ::std::fs::copy(from, to)?;
    Ok(())
}
