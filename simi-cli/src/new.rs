use clap;
use error::*;

#[derive(Debug)]
pub struct NewArgs {
    name: String,
}

impl NewArgs {
    pub fn from_clap<'a>(matches: &clap::ArgMatches<'a>) -> Self {
        Self {
            name: matches.value_of("NAME").unwrap().to_string(),
        }
    }
}

pub fn run(arg: NewArgs) -> Result<(), Error> {
    println!(
        "{:?}\n\n`simi new` is not implement yet! Do you want to help implementing this?",
        arg
    );
    Ok(())
}
