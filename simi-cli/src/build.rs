use std::fs;

use clap;

use config::SimiConfig;
use error::*;
use external_cmds as cmds;

pub struct BuildArgs {
    pub release: bool,
}

impl BuildArgs {
    pub fn from_clap<'a>(matches: &clap::ArgMatches<'a>) -> Self {
        Self {
            release: matches.is_present("release"),
        }
    }
}

pub fn build(config: &SimiConfig) -> Result<(), Error> {
    let nightly = true; // currently wasm only works with nightly
    cmds::cargo_build(config.release(), nightly, config.target())?;

    // Create output_path before running wasm-bindgen
    // wasm-bindgen don't like to create new folder
    if config.output_path().canonicalize().is_err() {
        fs::create_dir_all(&config.output_path())?;
    }
    cmds::wasm_bindgen(config)?;
    cmds::wasm_to_es6js(config)?;

    ::project::create_simi_app(&config)?;
    Ok(())
}

pub fn run(arg: BuildArgs) -> Result<(), Error> {
    let config = SimiConfig::from_build(arg)?;
    let _ = build(&config)?;
    Ok(())
}
