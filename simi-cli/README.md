# simi-cli

Provide `simi`, a cli tool for creating and building [`simi` frontend web app](https://crates.io/crates/simi).

`simi` command is built on a Fedora system. I am not sure if it works on other systems or not.

`simi build` build the app.
`simi serve` build and serve the app. If your code changes, it will not rebuild the app for you. You must stop it and run it again. (Any help to implement the `auto rebuild` feature is very appreciate).
`simi test` run tests in headless browser

## Output

`simi-cli` will output at least 4 files: `<app_name>.js`, `<app_name>_bg.js`, `<app_name>_bg.wasm` and `index.html`. Where `<app_name>` is the `package.name` in your `Cargo.toml` (replace any `-` by `_`).

All output files are placed in `crate-root/simi-site`, where `crate-root` is the folder where your `Cargo.toml` is.

## Static files
By default, `simi-cli` will generate a default `index.html`. If you want provide a `crate-root/static/style.scss`, `simi-cli` will compile them to `css` add a `<link>` to the generated `index.html`.

If you also want to provide your own `index.html`, you must place it in `crate-root/static/index.html`. And you must add

```
<script src='./<app_name>.js'></script>
```
and if you have a `crate-root/static/style.scss`, `simi-cli` still compile and output it to `simi-site`, so you also should add the following to your `index.html`
```
<link rel='stylesheet' href='./<style>.css'>
```

Other files in `crate-root/static` will be copied to `crate-root/simi-site` if they were modified.