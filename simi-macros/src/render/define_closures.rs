use super::raw_dom::*;
use super::{Config, CurrentParents};
use proc_macro2::*;

impl NodeList {
    pub fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        self.nodes
            .iter_mut()
            .for_each(|node| node.define_closures(config, closures));
    }
}

impl Node {
    fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        match self {
            Node::Element(ref mut e) => e.define_closures(config, closures),
            Node::Component(ref mut s) => s.define_closures(config, closures),
            Node::IfElse(ref mut ie) => ie.define_closures(config, closures),
            Node::Match(ref mut m) => m.define_closures(config, closures),
            _ => {}
        }
    }
}

impl Element {
    fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        self.content.define_closures(config, closures)
    }
}

impl ElementContent {
    fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        if let ElementContent::NodeList(ref mut nodes) = self {
            nodes.define_closures(config, closures)
        }
    }
}

impl Component {
    fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        self.childs.iter_mut().for_each(|child| {
            if let ComponentChildInfo::Element(ref mut e) = child.child {
                e.define_closures(config, closures)
            }
        })
    }
}

impl IfElse {
    fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        let closure_name = format!("generated_closure_number_{}_if_else", closures.len());

        self.arms
            .iter_mut()
            .enumerate()
            .for_each(|(index, arm)| arm.define_closures(config, closures, &closure_name, index));
    }
}

impl Match {
    fn define_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        let closure_name = format!("generated_closure_number_{}_match", closures.len());

        self.arms
            .iter_mut()
            .enumerate()
            .for_each(|(index, arm)| arm.define_closures(config, closures, &closure_name, index));
    }
}

impl Arm {
    fn define_closures(
        &mut self,
        config: &Config,
        closures: &mut Vec<TokenStream>,
        closure_name_prefix: &str,
        index: usize,
    ) {
        let create_closure_name = Ident::new(
            &format!("{}_arm_{}_create", closure_name_prefix, index),
            Span::call_site(),
        );
        let update_closure_name = Ident::new(
            &format!("{}_arm_{}_update", closure_name_prefix, index),
            Span::call_site(),
        );

        closures.push(self.define_create_closure(config, &create_closure_name));
        closures.push(self.define_update_closure(config, &update_closure_name));

        self.create_closure_name = Some(create_closure_name);
        self.update_closure_name = Some(update_closure_name);
    }

    fn define_create_closure(&self, config: &Config, name: &Ident) -> TokenStream {
        let app_type = config.get_app_type();
        let context_type = config.get_context_type();
        let simi_dom = &config.simi_dom_path;
        let nodes_list = quote!{nodes_list_in_an_if_else_closure_};
        let real_parent = quote!{real_parent_in_an_if_else_closure_};
        let next_sibling = quote!{next_sibling_for_an_if_else_closure_};
        let parents = CurrentParents::new_root(nodes_list.clone(), real_parent.clone())
            .insert_before_node(next_sibling.clone());

        let render = self.nodes.create(config, &parents);
        quote!{
            let #name = |
                #nodes_list: &mut #simi_dom::NodeList<#app_type>,
                #real_parent: &simi::interop::real_dom::RealElement,
                #next_sibling: Option<&simi::interop::real_dom::RealNode>,
                context: &#context_type| {
                #render
            };
        }
    }

    fn define_update_closure(&self, config: &Config, name: &Ident) -> TokenStream {
        let app_type = config.get_app_type();
        let context_type = config.get_context_type();
        let simi_dom = &config.simi_dom_path;
        let nodes_list = quote!{nodes_list_in_an_if_else_closure_};
        let real_parent = quote!{real_parent_in_an_if_else_closure_};
        let parents = CurrentParents::new_root(nodes_list.clone(), real_parent.clone());

        let update = self.nodes.update(config, &parents);
        quote!{
            let #name = |
                #nodes_list: &mut #simi_dom::NodeList<#app_type>,
                #real_parent: &simi::interop::real_dom::RealElement,
                context: &#context_type |
            {
                #update
            };
        }
    }
}
