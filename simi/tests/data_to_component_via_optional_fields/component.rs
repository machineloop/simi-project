use simi::prelude::*;

pub struct TestSnippet<'a, A: SimiApp> {
    pub title: Option<&'a String>,
    pub no_update_title: Option<&'a String>,
    pub props: Option<u32>,

    pub up: ElementEvent<A>,
    pub down: ElementEvent<A>,

    pub class1: Option<&'a String>,
    pub class2: Option<&'a String>,
    pub class_on: Option<bool>,

    pub input_value: Option<&'a String>,
    pub input_changed: ElementEvent<A>,

    pub checked: Option<bool>,
    pub no_update_checked: Option<bool>,
    pub input_alt: Option<&'a String>,
    pub no_update_input_alt: Option<&'a String>,
    pub check_changed: ElementEvent<A>,

    pub autofocus: Option<bool>,
    pub custom_attribute: Option<&'a String>,

    pub select_value: Option<&'a Option<String>>,
    pub select_index: Option<&'a Option<usize>>,
    pub select_value_changed: ElementEvent<A>,
    pub select_index_changed: ElementEvent<A>,
}

impl<'a, A: SimiApp> Component<A> for TestSnippet<'a, A> {
    type Properties = u32;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        let future: bool = *props > 2018;
        let class_name1 = "future-expr1";
        let class_name2 = "future-expr2";
        component! {
            //@debug
            button (id="down-button" onclick=self.down) { "Down" }
            button (id="up-button" onclick=self.up) { "Up" }

            div (id="no-update-by-snippet") { #self.title " " #props }
            div (id="no-update-by-caller") { self.no_update_title " " self.props }
            div (id="content-div") { self.title " " props }

            div (id="classes"
                "snippet-class"=self.class_on
                "future-literal"=future
                self.class1?=self.class_on
                self.class2?=future
                class_name1?=self.class_on
                class_name2?=future
            )

            input (id="input-text-value-no-update" value=#self.input_value)
            input (id="input-text-value" value=self.input_value onchange=self.input_changed)

            input (id="no-update-attribute-by-snippet" type="checkbox" checked=#self.checked alt=#self.input_alt)
            input (id="no-update-attribute-by-caller" type="checkbox" checked=self.no_update_checked  alt=self.no_update_input_alt)
            input (id="current-checked" type="checkbox" autofocus=self.autofocus checked=self.checked onchange=self.check_changed alt=self.input_alt)

            span (id="custom-attribute" data-no-update=#self.custom_attribute data-custom-data=self.custom_attribute) {"..?.."}

            select (id="select-value" value=self.select_value onchange=self.select_value_changed) {
                option (id="select-value-option1" value="first") { "First option" }
                option (id="select-value-option2" value="second") { "Second option" }
                option (id="select-value-option3" value="third") { "Third option" }
            }

            select (id="select-index" index=self.select_index onchange=self.select_index_changed) {
                option (id="select-index-option1" value="first") { "First option" }
                option (id="select-index-option2" value="second") { "Second option" }
                option (id="select-index-option3" value="third") { "Third option" }
            }
        }
    }
}

// Hmm...
// I gave up
// the support
// for testing
// a standalone component
// now!
// I maybe revisit this later!
