use simi::prelude::*;

use SnippetProps;

pub struct TestSnippet<A: SimiApp> {
    pub snippet_id: Option<&'static str>,
    pub up: ElementEvent<A>,
    pub down: ElementEvent<A>,
    pub child: ComponentChild<A>,
}

impl<A: SimiApp> Component<A> for TestSnippet<A> {
    type Properties = SnippetProps;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            //@debug
            div {
                p (
                    id=#format!("{}-content", self.snippet_id.unwrap())
                ) {
                    props.title ": " props.count
                }
                button (
                    id=#format!("{}-down-button", self.snippet_id.unwrap())
                    onclick=self.down
                ) {
                    "Down"
                }
                button (
                    id=#format!("{}-up-button", self.snippet_id.unwrap())
                    onclick=self.up
                ) {
                    "Up"
                }
            }
            $ self.child
        }
    }
}

pub struct TestSnippetChild<A: SimiApp> {
    pub input_id: Option<&'static str>,
    pub onchange: ElementEvent<A>,
}

impl<A: SimiApp> Component<A> for TestSnippetChild<A> {
    type Properties = SnippetProps;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            input (id=#self.input_id value=props.title onchange=self.onchange)
        }
    }
}

pub struct ForNestedThreeLevel<A: SimiApp> {
    pub child1: ComponentChild<A>,
    pub child2: ComponentChild<A>,
}

impl<A: SimiApp> Component<A> for ForNestedThreeLevel<A> {
    type Properties = ();
    fn render(&self, _props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            //@debug
            $ self.child1
            $ self.child2
        }
    }
}
