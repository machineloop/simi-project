#!/bin/bash

set -e

TARGET="--target=wasm32-unknown-unknown"

echo "Build all ./examples"
cargo +nightly build $TARGET --manifest-path=./examples/a-counter/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/b-counter-component/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/c-counter-nested-component/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/d-clock-ubu-component/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/e-clock-sub-app/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/todo/Cargo.toml
