# `Simi` Change Log

---
## v0.1.2
Released 2018-10-08.

### Fixed

* Rust nightly `#![feature(proc_macro_hygiene)]`

## v0.1.1
Released 2018-10-02.

### Fixed

* Correct version for simi-html-tags in simi-macros

## v0.1.0

Released 2018-10-01.

### Added

* Simi
    * Support for enum as app state
    * Todo example
    * No alias for element events

### Fixed

* Simi
    * Fixed `#[simi_app(element-id)]`


## Template
(Do not add to this section, please add change to the topmost section)

Released YYYY-MM-DD.

### Added

* TODO 

### Changed

* TODO 

### Deprecated

* TODO 

### Removed

* TODO 

### Fixed

* TODO 

### Security

* TODO 
