//! Binding for setInterval from js

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
/// set interval
extern "C" {
    #[wasm_bindgen(js_name = setInterval)]
    pub fn set_interval(cb: &Closure<Fn()>, delay: u32) -> f64;
}
