//! Binding for console from js

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
/// Console
extern "C" {
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    /// console.log for a `str`
    pub fn log(s: &str);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    /// console.log for a `str` + another `str`
    pub fn log_str(s: &str, more: &str);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    /// console.log for a `str` + another `u32`
    pub fn log_u32(s: &str, u: u32);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    /// console.log for a `str` + another `usize`
    pub fn log_usize(s: &str, u: usize);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    /// console.log for a `str` + another `bool`
    pub fn log_bool(s: &str, b: bool);

    #[wasm_bindgen(js_namespace = console, js_name = warn)]
    /// console.warn for a `str`
    pub fn warn(s: &str);
}
