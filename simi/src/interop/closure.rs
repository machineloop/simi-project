//! Create closures that interacts between js and simi

use app::{SimiApp, WeakMain};
use wasm_bindgen::closure::{Closure, WasmClosure};

/// Create closure for a handler that requires no argument
pub fn no_arg<A, H>(main: WeakMain<A>, handler: H) -> Closure<Fn()>
where
    A: SimiApp + 'static,
    H: Fn() -> A::Message + 'static,
{
    let closure = Closure::new(move || match main.upgrade() {
        None => ::interop::console::log("Unable to send message back to main now"),
        Some(main) => main.borrow_mut().update(handler()),
    });
    closure
}

/// Create closure for a handler that requires an argument
pub fn with_arg<A, T, H>(main: WeakMain<A>, handler: H) -> Closure<Fn(T)>
where
    A: SimiApp + 'static,
    H: Fn(T) -> A::Message + 'static,
    Fn(T) + 'static: WasmClosure,
{
    let closure = Closure::new(move |value: T| match main.upgrade() {
        None => ::interop::console::log("Unable to send message back to main now"),
        Some(main) => main.borrow_mut().update(handler(value)),
    });
    closure
}
