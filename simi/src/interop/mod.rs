//! This module supports interop with things provided by browsers
pub mod closure;
pub mod console;
pub mod interval;

/// Display error return by web-sys
pub fn display_error<R>(rs: Result<R, ::wasm_bindgen::JsValue>) {
    match rs {
        Ok(_) => {}
        Err(e) => {
            ::interop::console::log(&e.as_string().unwrap_or("No error message?".to_string()))
        }
    }
}

/// Reexport web_sys and have some aliases
pub mod real_dom {
    pub use web_sys::*;

    /// Alias to interop::real_dom::Element
    pub type RealElement = Element;
    /// Alias to interop::real_dom::Node
    pub type RealNode = Node;

    /// Document
    pub fn document() -> Document {
        window()
            .expect("Must have a window")
            .document()
            .expect("Must have a document")
    }
}
