//! Callbacks from JS to Rust

use app::{SimiApp, WeakMain};

/// A callback that send a message without data
pub type Callback = Box<Fn()>;
/// A callback that send a message with data
pub type CallbackArg<T> = Box<Fn(T)>;

/// Create callback for a handler that requires no argument
pub fn no_arg<A, H>(main: WeakMain<A>, handler: H) -> Callback
where
    A: SimiApp + 'static,
    H: Fn() -> A::Message + 'static,
{
    let callback = Box::new(move || match main.upgrade() {
        None => ::interop::console::log("Unable to send message back to main now"),
        Some(main) => main.borrow_mut().update(handler()),
    });
    callback
}

/// Create callback for a handler that requires an argument
pub fn with_arg<A, T, H>(main: WeakMain<A>, handler: H) -> CallbackArg<T>
where
    A: SimiApp + 'static,
    H: Fn(T) -> A::Message + 'static,
{
    let callback = Box::new(move |value: T| match main.upgrade() {
        None => ::interop::console::log("Unable to send message back to main now"),
        Some(main) => main.borrow_mut().update(handler(value)),
    });
    callback
}
