use super::{NodeList, RcComponentChild, SimiCreator};
use app::SimiApp;
use interop::real_dom::{self, RealElement};

/// A reusable component
pub struct Component<A: SimiApp> {
    /// Root nodes of a component
    nodes: NodeList<A>,
    /// Use only for a component-child.
    /// If its value is true, then the parent should rerender this.
    need_rerender: bool,
    /// Use for the child-component
    real_parent: Option<RealElement>,
    /// A hacky way to mark the place where `self` (as a child-component) will be inserted to
    place_holder: Option<RealElement>,
    /// Childs of this component
    childs: Vec<RcComponentChild<A>>,
}

impl<A: SimiApp> SimiCreator<A> for Component<A> {}

impl<A: SimiApp> Component<A> {
    /// Create new empty component
    pub fn new(child_count: usize) -> Self {
        Self {
            nodes: NodeList::new(),
            need_rerender: false,
            real_parent: None,
            place_holder: None,
            childs: Vec::with_capacity(child_count),
        }
    }

    /// Is this component need to rerrender
    pub fn need_rerender(&self) -> bool {
        self.need_rerender
    }

    /// Update need_rerender
    pub fn set_need_rerender(&mut self, value: bool) {
        self.need_rerender = value;
    }

    /// Reference to nodes
    pub fn nodes(&self) -> &NodeList<A> {
        &self.nodes
    }

    /// Mutable reference to nodes of the list
    pub fn nodes_mut(&mut self) -> &mut NodeList<A> {
        &mut self.nodes
    }

    /// Set the real parent for the component
    pub fn set_real_parent(&mut self, parent: &RealElement) {
        self.real_parent = Some(parent.clone());
    }

    /// Set placeholder for the component
    pub fn add_place_holder(&mut self) {
        let place_holder = ::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        let parent_node: &::interop::real_dom::Node = self
            .real_parent
            .as_ref()
            .expect("Real parent must available before adding place holder")
            .as_ref();
        ::interop::display_error(parent_node.append_child(place_holder.as_ref()));
        self.place_holder = Some(place_holder);
    }

    /// Take the place holder
    pub fn take_place_holder(&mut self) -> Option<RealElement> {
        self.place_holder.take()
    }

    /// Add a component as a child of self
    pub fn add_child_component(&mut self, child: Component<A>) {
        self.childs
            .push(::std::rc::Rc::new(::std::cell::RefCell::new(child)));
    }

    /// Immutable reference to childs
    pub fn childs(&self) -> &Vec<RcComponentChild<A>> {
        &self.childs
    }

    /// Mutable reference to childs
    pub fn childs_mut(&mut self) -> &mut Vec<RcComponentChild<A>> {
        &mut self.childs
    }

    /// Mutable reference to nodes and childs
    pub fn nodes_childs_mut(&mut self) -> (&mut NodeList<A>, &mut Vec<RcComponentChild<A>>) {
        (&mut self.nodes, &mut self.childs)
    }

    /// Mutable reference to nodes, real_parent
    pub fn nodes_mut_real_parent(&mut self) -> (&mut NodeList<A>, &RealElement) {
        (
            &mut self.nodes,
            self.real_parent
                .as_ref()
                .expect("Real parent must be setted"),
        )
    }

    /// Mutable reference to nodes and childs, real_parent
    pub fn nodes_childs_mut_real_parent(
        &mut self,
    ) -> (
        &mut NodeList<A>,
        &mut Vec<RcComponentChild<A>>,
        &RealElement,
    ) {
        (
            &mut self.nodes,
            &mut self.childs,
            self.real_parent
                .as_ref()
                .expect("Real parent must be setted"),
        )
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &real_dom::Node,
    ) -> Option<RealElement> {
        self.nodes.remove_and_get_next_sibling(real_parent)
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &real_dom::Node) {
        self.nodes.remove_real_node(real_parent)
    }
}
