//! Errors
/// Error report by simi
#[derive(Debug)]
pub enum Error {
    /// Error with a value of &str
    ErrorStr(&'static str),
}
