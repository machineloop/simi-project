//! A simple framework for building front-end apps in Rust. Inspires by [Yew](https://github.com/DenisKolodin/yew).
//!
//! ## Important:
//! `simi` only works with a set of supported html tags. If `simi` found out that
//! an identifier is not a supported html tag, (and if its name is not in CamelCase)
//! it will automatically be treated it as an expression and render as it as a text node.
//! If you believe there are valid html tags that must be supported by `simi` please
//! open an issue (or better: open a pull/merge request).
#![deny(missing_docs)]
#![cfg(target_arch = "wasm32")]
extern crate fnv;
extern crate wasm_bindgen;
extern crate web_sys;

pub extern crate simi_html_tags as html_tags;
extern crate simi_macros;

#[cfg(test)]
extern crate wasm_bindgen_test;
#[cfg(test)]
use wasm_bindgen_test::wasm_bindgen_test_configure;
#[cfg(test)]
wasm_bindgen_test_configure!(run_in_browser);

type HashMap<K, V> = fnv::FnvHashMap<K, V>;

pub mod app;
pub mod callback;
pub mod element_events;
pub mod error;
pub mod interop;
pub mod simi_dom;
pub mod test_helper;

pub mod prelude {
    //! Common use items for a simi app
    pub use app::*;
    pub use element_events::ElementEvent;
    pub use error::Error as SimiError;
    pub use simi_dom::NodeList;
    pub use simi_macros::*;
}
