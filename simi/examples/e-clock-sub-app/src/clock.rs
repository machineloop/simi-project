use simi;
use simi::interop::interval;
use simi::prelude::*;
use wasm_bindgen::prelude::*;

pub enum ClockMsg {
    Down,
}

pub struct ClockContext {
    _ticker: Closure<Fn()>,
    pub timeup: Option<::simi::callback::Callback>,
}

impl ClockContext {
    fn new(main: WeakMain<Clock>) -> Self {
        let closure = simi::interop::closure::no_arg(main.clone(), || ClockMsg::Down);
        interval::set_interval(&closure, 1000);
        Self {
            _ticker: closure,
            timeup: None,
        }
    }
}

pub struct Clock {
    interval: u32,
    current_value: u32,
}

impl SimiApp for Clock {
    type Message = ClockMsg;
    type Context = ClockContext;

    fn init(main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                interval: 4,
                current_value: 4,
            },
            ClockContext::new(main),
        )
    }

    fn update(&mut self, m: Self::Message, context: &mut Self::Context) -> UpdateView {
        match m {
            ClockMsg::Down => {
                self.current_value -= 1;
                if self.current_value == 0 {
                    self.current_value = self.interval;
                    if let Some(ref c) = context.timeup {
                        c();
                    }
                }
            }
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            fieldset {
                legend {"sub-app: " b { "Clock" } }
                p { "This sub app updates every 1 second" }
                p { "Current value: " self.current_value }
            }
        }
    }
}
