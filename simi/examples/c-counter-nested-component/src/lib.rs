#![feature(proc_macro_hygiene)]
extern crate simi;
extern crate wasm_bindgen;

use simi::prelude::*;
use wasm_bindgen::prelude::*;

struct Counter<A: SimiApp> {
    title: Option<&'static str>,
    up: simi::element_events::ElementEvent<A>,
    down: simi::element_events::ElementEvent<A>,
}

impl<A: SimiApp> Component<A> for Counter<A> {
    type Properties = i32;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component! {
            fieldset {
                legend { "component: " b { "Counter" } }
                div { #self.title props }
                button (onclick=#self.down) { "Down" }
                button (onclick=#self.up) { "Up" }
            }
        }
    }
}

struct TwoCounters<A: SimiApp> {
    child1: ComponentChild<A>,
    child2: ComponentChild<A>,
}

impl<A: SimiApp> Component<A> for TwoCounters<A> {
    type Properties = ();
    fn render(&self, _props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            //@debug
            fieldset{
                legend { "component: " b { "TwoCounters" } }
                p {
                    "The following two separate "
                    code { "Counter" }
                    " components are passed to here by the caller (CounterApp)."
                }
                p {"The next box is " b {"child1"} " that is passed to here by the caller."}
                // A placeholder for a child component must be prefix with `$` to disambiguate with an expression
                $ self.child1
                p {"The next box is " b {"child2"} " that is also passed to here by the caller."}
                $ self.child2
            }
        }
    }
}

enum Msg {
    Value1Up,
    Value1Down,
    Value2Up,
    Value2Down,
}

#[simi_app]
struct CounterApp {
    value1: i32,
    value2: i32,
}

impl SimiApp for CounterApp {
    type Message = Msg;
    type Context = ();
    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                value1: 2018,
                value2: 8,
            },
            (),
        )
    }
    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::Value1Up => self.value1 += 1,
            Msg::Value1Down => self.value1 -= 1,
            Msg::Value2Up => self.value2 += 1,
            Msg::Value2Down => self.value2 -= 1,
        }
        true
    }
    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            //@debug
            h2 { "TwoCounters" }
            p { "using " b {"child components"} }
            hr
            TwoCounters {
                child1: Counter (self.value1) {
                    title: "First value: "
                    up: #onclick=Msg::Value1Up
                    down: #onclick=Msg::Value1Down
                }
                child2: Counter (self.value2) {
                    title: "Second value: "
                    up: #onclick=Msg::Value2Up
                    down: #onclick=Msg::Value2Down
                }
            }
        }
    }
}
