//! enum HtmlTag and a equivalent HashSet to help simi-macros
extern crate fnv;

include!(concat!(env!("OUT_DIR"), "/html_tags.rs"));
