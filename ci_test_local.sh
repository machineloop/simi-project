#!/bin/bash

# Run this locally to make sure all tests pass and all examples build

set -e

TARGET="--target=wasm32-unknown-unknown"

echo "Enter simi-project/simi"
cd simi

./build_all_simi_examples.sh

echo "Run all simi-project/simi/tests"
simi test

echo "Clippying on simi-project/simi"
cargo +nightly clippy

echo "Enter simi-project/simi-macros"
cd ../simi-macros

echo "Run tests for simi-project/simi-macros"
cargo +nightly test

echo "Clippying on simi-project/simi-macros"
cargo +nightly clippy